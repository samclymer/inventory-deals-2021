import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
//import "./App.css";
import CurrentInventory from "../Pages/CurrentInventory.js";
import AdminDashboard from "../Pages/AdminDashboard.js";
import UserDashboard from "../Pages/UserDashboard.js";
import InventoryDetailsAdmin from "../Pages/InventoryDetailsAdmin.js";
import InventoryDetailsUser from "../Pages/InventoryDetailsUser.js";
import InventoryListAdmin from "../Pages/InventoryListAdmin.js";
import LandingPage from "../Pages/LandingPage.js";
import MessagesDetailsAdmin from "../Pages/MessagesDetailsAdmin.js";
import MessagesListAdmin from "../Pages/MessagesListAdmin.js";
import UserAccountDetails from "../Pages/UserAccountDetails.js";
import UserListAdmin from "../Pages/UserListAdmin.js";


function App() {
  return (
    <div>
      <Router>
        <div>
          <Switch>
            <Route exact path= "/" component={LandingPage} />
            <Route exact path= "/currentinventory" component={CurrentInventory} />
            <Route exact path= "/admindashboard" component={AdminDashboard} />
            <Route exact path= "/userdashboard" component={UserDashboard} />
            <Route exact path= "/inventorydetailsadmin" component={InventoryDetailsAdmin} />
            <Route exact path= "/inventorylistadmin" component={InventoryListAdmin} />
            <Route exact path= "/userlistadmin" component={UserListAdmin} />
            <Route exact path= "/useraccountdetails" component={UserAccountDetails} />
            <Route exact path= "/messageslistadmin" component={MessagesListAdmin} />
            <Route exact path= "/messagesdetailsadmin" component={MessagesDetailsAdmin} />
            <Route exact path= "/inventorydetailsuser" component={InventoryDetailsUser} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
