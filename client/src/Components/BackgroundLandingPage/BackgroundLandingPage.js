import React from "react";
import { Container } from "react-bootstrap";
import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./BackgroundLandingPage.module.css";

function BackgroundLandingPage() {
  return (
    <Container fluid="lg" className={styles.videocontainer}>
      <video
        className={cx(styles.videohelp)}
        autoPlay
        muted
        loop
        playsinline="true"
        disablePictureInPicture="true"
        id="myVideo"
      >
        <source src={require("./9190772-sd.mov").default} type="video/mp4" />
        Video Playback Unavailable
      </video>
      <div className={cx(styles.jumboassist)}></div>
    </Container>
  );
}

export default BackgroundLandingPage;
