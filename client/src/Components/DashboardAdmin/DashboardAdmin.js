import React from "react";
import { Container } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./DashboardAdmin.module.css";

function DashboardAdmin() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Administrative Dashboard</h1>
        <hr className={styles.divider} />
      </div>
      <div className="row row-cols-1 row-cols-md-3 mb-3 pt-2 text-center">
        <div className="col">
          <a href="/inventorylistadmin" style={{textDecoration: "none"}}>
            <div className="card mb-4 h-100 border-0">
              <div className="card-header bg-transparent border-0">
                <h4 className="my-0 fw-normal text-dark">Inventory</h4>
              </div>
              <div className="card-body">
                <i className="fas fa-box-open text-dark" style={{ fontSize: "11vmin" }}></i>
              </div>
            </div>
          </a>
        </div>
        <div className="col">
          <a href="/userlistadmin" style={{textDecoration: "none"}}>
            <div className="card mb-4 h-100 border-0">
              <div className="card-header bg-transparent border-0">
                <h4 className="my-0 fw-normal text-dark">Users</h4>
              </div>
              <div className="card-body">
                <i className="fas fa-users text-dark" style={{ fontSize: "11vmin" }}></i>
              </div>
            </div>
          </a>
        </div>
        <div className="col">
          <a href="/messageslistadmin" style={{textDecoration: "none"}}>
            <div className="card mb-4 h-100 border-0">
              <div className="card-header bg-transparent border-0">
                <h4 className="my-0 fw-normal text-dark">Messages</h4>
              </div>
              <div className="card-body">
                <i className="fas fa-envelope text-dark" style={{ fontSize: "11vmin" }}></i>
              </div>
            </div>
          </a>
        </div>
      </div>
    </Container>
  );
}

export default DashboardAdmin;
