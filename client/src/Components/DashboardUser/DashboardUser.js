import React from "react";
import { Container } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./DashboardUser.module.css";

function DashboardUser() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>User Dashboard</h1>
        <hr className={styles.divider} />
      </div>
      <div className="row row-cols-1 row-cols-md-2 mb-3 pt-2 text-center">
        <div className="col">
          <a href="/useraccountdetails" style={{ textDecoration: "none" }}>
            <div className="card mb-4 h-100 border-0">
              <div className="card-header bg-transparent border-0">
                <h4 className="my-0 fw-normal text-dark">My Account</h4>
              </div>
              <div className="card-body">
                <i className="fas fa-user text-dark" style={{ fontSize: "11vmin" }}></i>
              </div>
            </div>
          </a>
        </div>
        <div className="col">
          <a href="/currentinventory" style={{ textDecoration: "none" }}>
            <div className="card mb-4 h-100 border-0">
              <div className="card-header bg-transparent border-0">
                <h4 className="my-0 fw-normal text-dark">Saved List</h4>
              </div>
              <div className="card-body">
                <i className="fas fa-box-open text-dark" style={{ fontSize: "11vmin" }}></i>
              </div>
            </div>
          </a>
        </div>
      </div>
    </Container>
  );
}

export default DashboardUser;
