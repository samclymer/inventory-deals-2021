import React from "react";
import { Container } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainCurrentInventory.module.css";

function MainCurrentInventory() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1>Current Inventory</h1>
        <hr className={styles.divider} />
      </div>
      <div className="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3">
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/bags.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong className={styles.colorfix}>Women's Handbags</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 6893
                </h6>
                <h6>
                  <strong>Posted:</strong> 12/18/2021
                </h6>
                <h6>
                  <strong>Location:</strong> San Jose, CA
                </h6>
                <h6>
                  <strong>Shipping:</strong> 4 Pallets / 650lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck1"
                  />
                  <label className="custom-control-label" for="customCheck1">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/dumbbells.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Fitness Equipment</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 7441
                </h6>
                <h6>
                  <strong>Posted:</strong> 12/07/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Buffalo, NY
                </h6>
                <h6>
                  <strong>Shipping:</strong> 8 Pallets / 7500lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck2"
                  />
                  <label className="custom-control-label" for="customCheck2">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/marbles.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Marbles</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 1516
                </h6>
                <h6>
                  <strong>Posted:</strong> 12/01/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Houston, TX
                </h6>
                <h6>
                  <strong>Shipping:</strong> 3 Pallets / 3000lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck3"
                  />
                  <label className="custom-control-label" for="customCheck3">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/neckties.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Men's Neckties</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 9889
                </h6>
                <h6>
                  <strong>Posted:</strong> 11/21/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Stone Mountain, GA
                </h6>
                <h6>
                  <strong>Shipping:</strong> 1 Pallets / 400lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck4"
                  />
                  <label className="custom-control-label" for="customCheck4">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/supermarket.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Potato Chips (Variety)</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 4890
                </h6>
                <h6>
                  <strong>Posted:</strong> 11/09/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Nashville, TN
                </h6>
                <h6>
                  <strong>Shipping:</strong> 2 Pallets / 700lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck5"
                  />
                  <label className="custom-control-label" for="customCheck5">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/warehouse.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Concrete Mix</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 4460
                </h6>
                <h6>
                  <strong>Posted:</strong> 10/25/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Boston, MA
                </h6>
                <h6>
                  <strong>Shipping:</strong> 12 Pallets / 18000lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck6"
                  />
                  <label className="custom-control-label" for="customCheck6">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/hats.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Women's Derby Hats</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 5700
                </h6>
                <h6>
                  <strong>Posted:</strong> 10/18/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Louisville, KY
                </h6>
                <h6>
                  <strong>Shipping:</strong> 4 Pallets / 1600lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck7"
                  />
                  <label className="custom-control-label" for="customCheck7">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div className="col mb-4">
          <a href="/inventorydetailsuser">
            <div className="card h-100 shadow">
              <img
                src={require("../../Assets/images/vitamins.jpg").default}
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title">
                  <strong>Fish Oil Supplements</strong>
                </h6>
                <hr />
                <h6>
                  <strong>Lot#:</strong> 1087
                </h6>
                <h6>
                  <strong>Posted:</strong> 10/02/2021
                </h6>
                <h6>
                  <strong>Location:</strong> Orlando, FL
                </h6>
                <h6>
                  <strong>Shipping:</strong> 2 Pallets / 1600lbs
                </h6>
                <hr />
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheck8"
                  />
                  <label className="custom-control-label" for="customCheck8">
                    Add to Saved List
                  </label>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </Container>
  );
}

export default MainCurrentInventory;
