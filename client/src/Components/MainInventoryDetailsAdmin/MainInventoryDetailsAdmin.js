import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Modal } from "react-bootstrap";
//import cx from "classnames";
import bsCustomFileInput from "bs-custom-file-input";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainInventoryDetailsAdmin.module.css";

function MainInventoryDetailsAdmin() {
  const [showDelete, setShow1] = useState(false);

  const handleCloseDelete = () => setShow1(false);
  const handleShowDelete = () => setShow1(true);

  useEffect(() => {
    bsCustomFileInput.init();
  }, []);

  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Create/Edit Inventory</h1>
        <hr className={styles.divider} />
      </div>
      <Form>
        <Row>
          <Col md={2}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Lot #</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={7}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Title</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={3}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Posted Date</Form.Label>
              <Form.Control type="date" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Location</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={3}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Quantity</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Condition</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Shipping</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Payment / Terms</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Form.Label>Attach Manifest</Form.Label>
            <Form.File id="custom-file" label="" custom />
          </Col>
        </Row>
        <Row className={styles.buttonrow}>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="secondary"
              size="lg"
              href="/inventorylistadmin"
            >
              Cancel
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="primary"
              size="lg"
              href="/inventorydetailsadmin"
            >
              Save
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              onClick={handleShowDelete}
              variant="danger"
              size="lg"
            >
              Delete
            </Button>
          </Col>
        </Row>
      </Form>
      <Modal
        show={showDelete}
        onHide={handleCloseDelete}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>
            This action will delete the current inventory lot - Are you sure you
            want to continue?
          </Modal.Title>
        </Modal.Header>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDelete}>
            Cancel
          </Button>
          <Button variant="danger" type="submit" href="inventorydetailsadmin">
            Delete Lot
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default MainInventoryDetailsAdmin;
