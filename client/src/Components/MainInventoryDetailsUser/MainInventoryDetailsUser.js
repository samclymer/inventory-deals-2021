import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Carousel,
  Button,
  Modal,
  Form,
} from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainInventoryDetailsUser.module.css";

function MainInventoryDetailsUser() {
  const [showDelete, setShow1] = useState(false);

  const handleCloseDelete = () => setShow1(false);
  const handleShowDelete = () => setShow1(true);

  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Lot #6893 Details</h1>
        <hr className={styles.divider} />
      </div>
      <Row>
        <Col lg={6}>
          <Carousel className="shadow">
            <Carousel.Item interval={2000}>
              <img
                className="d-block w-100"
                src={require("../../Assets/images/bags.jpg").default}
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item interval={2000}>
              <img
                className="d-block w-100"
                src={require("../../Assets/images/bags2.jpg").default}
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item interval={2000}>
              <img
                className="d-block w-100"
                src={require("../../Assets/images/bags3.jpg").default}
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </Col>
        <Col lg={6} className="cushion">
          <div class="card border-0">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h4 class="card-title border-bottom border-gray pb-3">
                    Women's Handbags
                  </h4>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Posted:</strong>
                    12/18/2020
                  </p>
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Location:</strong>
                    San Jose, CA
                  </p>
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">
                      Payment / Terms:
                    </strong>
                    50% Deposit with balance due within 30 days upon delivery
                  </p>
                </div>
                <div class="col-lg-6">
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Quantity:</strong>
                    160 units (5 units per case / 8 cases per pallet)
                  </p>
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Shipping:</strong>4
                    pallets - 650lbs
                  </p>
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Condition:</strong>
                    New in Original Packaging
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Description:</strong>
                    This lot contains a variety of top name brand handbags,
                    sourced from Federated Department Stores (Macy's /
                    Bloomingdales).
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
      <Row className={styles.buttonrow}>
        <Col className={styles.buttoncol}>
          <Button
            className={styles.buttonhelp}
            onClick={handleShowDelete}
            variant="success"
            size="lg"
          >
            Ask Question
          </Button>
        </Col>
      </Row>
      <Modal
        show={showDelete}
        onHide={handleCloseDelete}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Form>
              <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>Ask Question Below</Form.Label>
                <Form.Control as="textarea" rows={3} cols={100} />
                <h6>(Please allow up to 48 hours for response)</h6>
              </Form.Group>
            </Form>
          </Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDelete}>
            Cancel
          </Button>
          <Button variant="primary" type="submit" href="/inventorydetailsuser">
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
      <Row>
        <Col lg={12} className="cushion">
          <div class="card border-0">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h4 class="card-title border-bottom border-gray pb-3">
                    Questions & Answers
                  </h4>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Question:</strong>
                    Does this lot include any Michael Kors handbags?
                  </p>
                </div>
                <div class="col-lg-12 mb-2">
                  <p class="media-body pb-3 mb-0 medium lh-125 border-bottom border-gray">
                    <strong class="d-block text-gray-dark">Answer:</strong>
                    Yes, this lot includes Michael Kors, Kate Spade and Tory Burch.
                  </p>
                </div>
                <div class="col-lg-12">
                  <p class="media-body pb-3 mb-0 medium lh-125">
                    <strong class="d-block text-gray-dark">Question:</strong>
                    About how many different styles of handbags are in this partiduclar lot?
                  </p>
                </div>
                <div class="col-lg-12 mb-2">
                  <p class="media-body pb-3 mb-0 medium lh-125 border-bottom border-gray">
                    <strong class="d-block text-gray-dark">Answer:</strong>
                    40% of the bags are medium-sized crossbody, and bout 60% are medium and large totes.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default MainInventoryDetailsUser;
