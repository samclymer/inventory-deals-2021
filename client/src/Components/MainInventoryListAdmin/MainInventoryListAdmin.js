import React from "react";
import { Container, Button, Table } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainInventoryListAdmin.module.css";

function MainInventoryListAdmin() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Inventory Database</h1>
        <hr className={styles.divider} />
      </div>
      <div className={styles.titletext}>
        <Button
          className={styles.buttonhelp}
          variant="success"
          size="lg"
          href="/inventorydetailsadmin"
        >
          Create New Listing
        </Button>
        
      </div>
      <Table responsive="xl" className="text-center">
      <thead>
          <tr>
            <th scope="col">EDIT</th>
            <th scope="col">Lot#</th>
            <th scope="col">Title</th>
            <th scope="col">Location</th>
            <th scope="col">Condition</th>
            <th scope="col">Quantity</th>
            <th scope="col">Date Posted</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><Button size="sm" variant="warning" href="/inventorydetailsadmin">EDIT</Button></td>
            <td>6893</td>
            <td>Women's Handbags</td>
            <td>San Jose, CA</td>
            <td>Customer Returns</td>
            <td>2000</td>
            <td>12/18/20</td>
          </tr>
          <tr>
            <td><Button size="sm" variant="warning" href="/inventorydetailsadmin">EDIT</Button></td>
            <td>7441</td>
            <td>Fitness Equipment</td>
            <td>Buffalo, NY</td>
            <td>New</td>
            <td>324</td>
            <td>12/7/20</td>
          </tr>
          <tr>
            <td><Button size="sm" variant="warning" href="/inventorydetailsadmin">EDIT</Button></td>
            <td>1516</td>
            <td>Marbles</td>
            <td>Houston, TX</td>
            <td>New</td>
            <td>12000</td>
            <td>12/1/20</td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
}

export default MainInventoryListAdmin;
