import React, { useState } from "react";
import { Container, Button, Modal, Form } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainLandingPage.module.css";

function MainLandingPage() {
  const [showLogIn, setShow1] = useState(false);
  const [showSignUp, setShow2] = useState(false);

  const handleCloseLogIn = () => setShow1(false);
  const handleShowLogIn = () => setShow1(true);
  const handleCloseSignUp = () => setShow2(false);
  const handleShowSignUp = () => setShow2(true);

  return (
    <Container>
      <main role="main" className={styles.cover}>
        <h1 className="cover-heading">
          Welcome to Inventory Deals!
        </h1>
        <p className={styles["small-text"]}>
          We are currently under construction - Check back soon for updates!
        </p>
        <span>
          <div className="mb-2">
            <Button
              className={styles["main-buttons"]}
              variant="secondary"
              size="lg"
              onClick={handleShowLogIn}
            >
              Log In
            </Button>{" "}
            <Button
              className={styles["main-buttons"]}
              variant="secondary"
              size="lg"
              onClick={handleShowSignUp}
            >
              Sign Up
            </Button>
          </div>
        </span>
        <Modal
          show={showLogIn}
          onHide={handleCloseLogIn}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Log into your account</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseLogIn}>
              Cancel
            </Button>
            <Button variant="primary" type="submit" href="/currentinventory">
              Sign In
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showSignUp}
          onHide={handleCloseSignUp}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Create an account</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="email" />
              </Form.Group>

              <Form.Group controlId="formLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="password" />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseSignUp}>
              Cancel
            </Button>
            <Button variant="primary" type="submit"href="/currentinventory" >
              Create Account
            </Button>
          </Modal.Footer>
        </Modal>
      </main>
    </Container>
  );
}

export default MainLandingPage;
