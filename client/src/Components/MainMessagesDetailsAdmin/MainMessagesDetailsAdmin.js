import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Modal } from "react-bootstrap";
//import cx from "classnames";
import bsCustomFileInput from "bs-custom-file-input";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainMessagesDetailsAdmin.module.css";

function MainMessagesDetailsAdmin() {
  const [showDelete, setShow1] = useState(false);

  const handleCloseDelete = () => setShow1(false);
  const handleShowDelete = () => setShow1(true);

  useEffect(() => {
    bsCustomFileInput.init();
  }, []);

  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Message Details</h1>
        <hr className={styles.divider} />
      </div>
      <Form>
        <Row>
          <Col md={2}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Lot#</Form.Label>
              <Form.Control type="text" placeholder="9889" disabled/>
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Title</Form.Label>
              <Form.Control type="text" placeholder="Men's Neckties" disabled/>
            </Form.Group>
          </Col>
          <Col md={3}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Date Asked</Form.Label>
              <Form.Control type="text" placeholder="12/7/20" disabled/>
            </Form.Group>
          </Col>
          <Col md={3}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Date Answered</Form.Label>
              <Form.Control type="text" placeholder="---" disabled/>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Question</Form.Label>
              <Form.Control as="textarea" rows={3} placeholder="Can you provide some of the brands I would expect to receive with this listing?" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Answer</Form.Label>
              <Form.Control as="textarea" rows={3} />
            </Form.Group>
          </Col>
        </Row>
        <Row className={styles.buttonrow}>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="secondary"
              size="lg"
              href="/messageslistadmin"
            >
              Cancel
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="primary"
              size="lg"
              href="/messageslistadmin"
            >
              Save
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              onClick={handleShowDelete}
              variant="danger"
              size="lg"
            >
              Delete
            </Button>
          </Col>
        </Row>
      </Form>
      <Modal
        show={showDelete}
        onHide={handleCloseDelete}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>
            This action will delete the current user - Are you sure you
            want to continue?
          </Modal.Title>
        </Modal.Header>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDelete}>
            Cancel
          </Button>
          <Button variant="danger" type="submit" href="/messageslistadmin">
            Delete Lot
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default MainMessagesDetailsAdmin;
