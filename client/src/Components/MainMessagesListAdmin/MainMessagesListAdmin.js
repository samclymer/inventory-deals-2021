import React from "react";
import { Container, Button, Table } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainMessagesListAdmin.module.css";

function MainMessagesListAdmin() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Messages</h1>
      </div>

      <Table responsive="xl" className="text-center">
        <thead>
          <tr>
            <th scope="col">ANSWER/EDIT</th>
            <th scope="col">Lot#</th>
            <th scope="col">Title</th>
            <th scope="col">Message</th>
            <th scope="col">Asked</th>
            <th scope="col">Answered</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Button size="sm" variant="info" href="/messagesdetailsadmin">
                ANSWER
              </Button>
            </td>
            <td>9889</td>
            <td>Men's Neckties</td>
            <td>
              Can you provide some of the brands I would expect to receive with
              this listing?
            </td>
            <td>12/7/20</td>
            <td>---</td>
          </tr>
          <tr>
            <td>
              <Button size="sm" variant="info" href="/messagesdetailsadmin">
                ANSWER
              </Button>
            </td>
            <td>4890</td>
            <td>Potato Chips (Variety)</td>
            <td>What are the expiration dates on this lot?</td>
            <td>11/18/20</td>
            <td>---</td>
          </tr>
          <tr>
            <td>
              <Button size="sm" variant="warning" href="/messagesdetailsadmin">
                EDIT
              </Button>
            </td>
            <td>4460</td>
            <td>Concrete Mix</td>
            <td>
              Can available load fit in one shipping container if were to
              receive in UK?
            </td>
            <td>12/1/20</td>
            <td>12/2/20</td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
}

export default MainMessagesListAdmin;
