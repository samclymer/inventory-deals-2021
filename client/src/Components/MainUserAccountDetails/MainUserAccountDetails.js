import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Modal } from "react-bootstrap";
//import cx from "classnames";
import bsCustomFileInput from "bs-custom-file-input";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainUserAccountDetails.module.css";

function MainUserAccountDetails() {
  const [showDelete, setShow1] = useState(false);

  const handleCloseDelete = () => setShow1(false);
  const handleShowDelete = () => setShow1(true);

  useEffect(() => {
    bsCustomFileInput.init();
  }, []);

  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Account Details</h1>
        <hr className={styles.divider} />
      </div>
      <Form>
        <Row>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>First Name</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Last Name</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Phone</Form.Label>
              <Form.Control type="tel" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Tax ID</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
          <Form.Group controlId="formBasicEmail">
              <Form.Label>Address</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
        <Col md={12}>
          <Form.Group controlId="formBasicEmail">
              <Form.Label>Address2</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>City</Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>
          </Col>
          <Col md={4}>
          <Form.Group as={Col} controlId="formGridState">
      <Form.Label>State</Form.Label>
      <Form.Control as="select" defaultValue="Choose...">
        <option>Choose...</option>
        <option>...</option>
      </Form.Control>
    </Form.Group>
          </Col>
          <Col md={2}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Zip</Form.Label>
              <Form.Control type="date" placeholder="" />
            </Form.Group>
          </Col>
        </Row>
        <Row className={styles.buttonrow}>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="secondary"
              size="lg"
              href="/userlistadmin"
            >
              Cancel
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              variant="primary"
              size="lg"
              href="/useraccountdetails"
            >
              Save
            </Button>
          </Col>
          <Col md={4} className={styles.buttoncol}>
            <Button
              className={styles.buttonhelp}
              onClick={handleShowDelete}
              variant="danger"
              size="lg"
            >
              Delete
            </Button>
          </Col>
        </Row>
      </Form>
      <Modal
        show={showDelete}
        onHide={handleCloseDelete}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>
            This action will delete the current user - Are you sure you
            want to continue?
          </Modal.Title>
        </Modal.Header>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDelete}>
            Cancel
          </Button>
          <Button variant="danger" type="submit" href="/useraccountdetails">
            Delete Lot
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default MainUserAccountDetails;
