import React from "react";
import { Container, Button, Table } from "react-bootstrap";
//import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./MainUserListAdmin.module.css";

function MainUserListAdmin() {
  return (
    <Container className={styles.mainframe}>
      <div className={styles.titletext}>
        <h1 className={styles.pagetitle}>Users Database</h1>
        <hr className={styles.divider} />
      </div>
      <div className={styles.titletext}>
        <Button
          className={styles.buttonhelp}
          variant="success"
          size="lg"
          href="/useraccountdetails"
        >
          Create New User
        </Button>
        
      </div>
      <Table responsive="xl"className="text-center" >
      <thead>
          <tr>
            <th scope="col">EDIT</th>
            <th scope="col">ID#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Email</th>
            <th scope="col">Password</th>
            <th scope="col">Address</th>
            <th scope="col">Address2</th>
            <th scope="col">City</th>
            <th scope="col">State</th>
            <th scope="col">Zip</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><Button size="sm" variant="warning" href="/useraccountdetails">EDIT</Button></td>
            <td>322</td>
            <td>Rhett</td>
            <td>Fowler</td>
            <td>rhettisthecoolest@gmail.com</td>
            <td>RH799UBK42</td>
            <td>123 Easy Street</td>
            <td></td>
            <td>Melbourne</td>
            <td>FL</td>
            <td>32901</td>
          </tr>
          <tr>
            <td><Button size="sm" variant="warning" href="/useraccountdetails">EDIT</Button></td>
            <td>109</td>
            <td>LJ</td>
            <td>Fowler</td>
            <td>ljistrouble@hotmail.com</td>
            <td>PP0113RT54</td>
            <td>4455 Tomfoolery Lane</td>
            <td></td>
            <td>Melbourne</td>
            <td>FL</td>
            <td>32905</td>
          </tr>
          <tr>
            <td><Button size="sm" variant="warning" href="/useraccountdetails">EDIT</Button></td>
            <td>782</td>
            <td>Michael</td>
            <td>Jordan</td>
            <td>mj23@yahoo.com</td>
            <td>WWQQTT24KL</td>
            <td>1001 United Center Drive</td>
            <td>Unit #345</td>
            <td>Chicago</td>
            <td>IL</td>
            <td>60007</td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
}

export default MainUserListAdmin;
