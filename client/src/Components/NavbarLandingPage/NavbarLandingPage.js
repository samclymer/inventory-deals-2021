import React from "react";
import { Navbar } from "react-bootstrap";
import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./NavbarLandingPage.module.css";

function NavbarLandingPage() {
  return (
    <Navbar collapseOnSelect expand="lg" className={styles.navhelp}>
      <Navbar.Brand href="/" className={cx(styles.navlogo, styles.logodiv)}>
        <img
          alt="logo"
          className={styles.logoresponsive}
          src={require("./Inventory-Deals-LOGO-2021-White.png").default}
        ></img>
      </Navbar.Brand>
    </Navbar>
  );
}

export default NavbarLandingPage;
