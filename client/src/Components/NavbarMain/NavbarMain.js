import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import cx from "classnames";
import "../../Assets/bootstrap/bootstrap.min.css";
import styles from "./NavbarMain.module.css";

function NavbarMain() {
  return (
    <Navbar collapseOnSelect expand="lg" className={cx(styles.navhelp, styles.shadowbar)}>
      <Navbar.Brand href="/" className={cx(styles.logodiv, styles.navlogo)}>
        <img
          alt="logo"
          className={styles.logoresponsive}
          src={require("./Inventory-Deals-LOGO-2021-Black.png").default}
        ></img>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto"></Nav>
        <Nav className="mr-5">
          <Nav.Link className={styles.navtext} href="/currentinventory">
            Current Inventory
          </Nav.Link>
          <Nav.Link className={styles.navtext} href="/admindashboard">
            Dashboard
          </Nav.Link>
          <Nav.Link className={styles.navtext} href="/">
            Log Out
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavbarMain;
