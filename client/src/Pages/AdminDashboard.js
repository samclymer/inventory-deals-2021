import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import DashboardAdmin from "../Components/DashboardAdmin/DashboardAdmin.js"


function AdminDashboard() {
  return (
    <div>
      <NavbarMain />
      <DashboardAdmin />
      </div>
  );
}

export default AdminDashboard;