import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainCurrentInventory from "../Components/MainCurrentInventory/MainCurrentInventory.js"


function CurrentInventory() {
  return (
    <div>
      <NavbarMain />
      <MainCurrentInventory />
      </div>
  );
}

export default CurrentInventory;