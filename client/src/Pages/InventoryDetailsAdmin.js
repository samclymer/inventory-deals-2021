import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainInventoryDetailsAdmin from "../Components/MainInventoryDetailsAdmin/MainInventoryDetailsAdmin.js"


function InventoryDetailsAdmin() {
  return (
    <div>
      <NavbarMain />
      <MainInventoryDetailsAdmin />
      </div>
  );
}

export default InventoryDetailsAdmin;