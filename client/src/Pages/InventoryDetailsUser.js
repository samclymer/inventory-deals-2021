import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainInventoryDetailsUser from "../Components/MainInventoryDetailsUser/MainInventoryDetailsUser.js"


function InventoryDetailsUser() {
  return (
    <div>
      <NavbarMain />
      <MainInventoryDetailsUser />
      </div>
  );
}

export default InventoryDetailsUser;