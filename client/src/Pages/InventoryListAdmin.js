import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainInventoryListAdmin from "../Components/MainInventoryListAdmin/MainInventoryListAdmin.js";


function InventoryListAdmin() {
  return (
    <div>
      <NavbarMain />
      <MainInventoryListAdmin />
      </div>
  );
}

export default InventoryListAdmin;