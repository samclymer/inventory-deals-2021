import React from "react";
import NavbarLandingPage from "../Components/NavbarLandingPage/NavbarLandingPage.js";
import MainLandingPage from "../Components/MainLandingPage/MainLandingPage.js";
import BackgroundLandingPage from "../Components/BackgroundLandingPage/BackgroundLandingPage.js";

function LandingPage() {
  return (
    <div>
      <NavbarLandingPage />
      <BackgroundLandingPage />
      <MainLandingPage />
      </div>
  );
}

export default LandingPage;