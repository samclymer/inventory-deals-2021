import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainMessagesDetailsAdmin from "../Components/MainMessagesDetailsAdmin/MainMessagesDetailsAdmin.js"


function MessagesDetailsAdmin() {
  return (
    <div>
      <NavbarMain />
      <MainMessagesDetailsAdmin />
      </div>
  );
}

export default MessagesDetailsAdmin;