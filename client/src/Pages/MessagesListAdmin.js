import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainMessagesListAdmin from "../Components/MainMessagesListAdmin/MainMessagesListAdmin.js"


function MessagesListAdmin() {
  return (
    <div>
      <NavbarMain />
      <MainMessagesListAdmin />
      </div>
  );
}

export default MessagesListAdmin;