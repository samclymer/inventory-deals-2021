import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainUserAccountDetails from "../Components/MainUserAccountDetails/MainUserAccountDetails.js"


function UserAccountDetails() {
  return (
    <div>
      <NavbarMain />
      <MainUserAccountDetails />
      </div>
  );
}

export default UserAccountDetails;