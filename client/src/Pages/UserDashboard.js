import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import DashboardUser from "../Components/DashboardUser/DashboardUser.js"


function UserDashboard() {
  return (
    <div>
      <NavbarMain />
      <DashboardUser />
      </div>
  );
}

export default UserDashboard;