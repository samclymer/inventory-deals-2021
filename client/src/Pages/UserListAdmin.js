import React from "react";
import NavbarMain from "../Components/NavbarMain/NavbarMain.js";
import MainUserListAdmin from "../Components/MainUserListAdmin/MainUserListAdmin.js";


function UserListAdmin() {
  return (
    <div>
      <NavbarMain />
      <MainUserListAdmin />
      </div>
  );
}

export default UserListAdmin;